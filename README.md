**Step 1** - Cài đặt và chạy Docker.

 - Win 10: [Download](https://store.docker.com/editions/community/docker-ce-desktop-windows)
 - Win < 10: [Download](https://www.docker.com/products/docker-toolbox)

> Nhớ **set proxy** cho Docker & environment variables trên máy nếu cần thiết

**Step 2** - Clone repo này về.

**Step 3** - Đứng ở thư mục gốc, chạy `docker-compose run composer` để cài đặt các thư viện trước.

**Step 4** - Sau khi cài xong, chạy tiếp `docker-compose up` chạy các services

**Step 5** - Các service

 - Web - http://localhost:8000
 - API - http://localhost:8001
 - Mysql - **127.0.0.1:3307**
 - MongoDB - **127.0.0.1:27018**
 - Redis - **127.0.0.1:6380**