<?php

use yii\db\Migration;

/**
 * Handles adding auth_key_column_password_hash_column_password_reset_token to table `users`.
 */
class m170911_083831_add_auth_key_column_activation_token_column_password_hash_column_password_reset_token_column_to_users_table extends Migration
{
    const TABLE_NAME = '{{%users}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'auth_key', $this->string(32)->notNull());
        $this->addColumn(self::TABLE_NAME, 'activation_token', $this->string()->unique());
        $this->addColumn(self::TABLE_NAME, 'password_hash', $this->string());
        $this->addColumn(self::TABLE_NAME, 'password_reset_token', $this->string()->unique());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'auth_key');
        $this->dropColumn(self::TABLE_NAME, 'activation_token');
        $this->dropColumn(self::TABLE_NAME, 'password_hash');
        $this->dropColumn(self::TABLE_NAME, 'password_reset_token');
    }
}
