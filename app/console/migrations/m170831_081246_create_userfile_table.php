<?php

use yii\db\Migration;

/**
 * Handles the creation of table `userfile`.
 */
class m170831_081246_create_userfile_table extends Migration
{
    const TABLE_NAME = '{{%userfile}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'COLLATE utf8mb4_general_ci';
        }

        $this->createTable(self::TABLE_NAME, [
            'id' => $this->bigPrimaryKey()->unsigned(),
            'linkcode' => $this->string(20)->unique()->notNull()->comment('lenght :12- create, 15-clone, 16-copy'),
            'name' => $this->string()->notNull(),
            'secure' => $this->integer()->defaultValue(0)->comment('0: not secured; 1: secured'),
            'public' => $this->integer()->defaultValue(1)->comment('-1: private; 1: public'),
            'copied' => $this->integer()->defaultValue(0)->comment('0: file root; or oid'),
            'shared' => $this->integer()->defaultValue(0)->comment('0: not share; 1: file share'),
            'directlink' => $this->integer()->defaultValue(0)->comment('0: not directlink; 1: directlink; 2: special'),
            'type' => $this->integer()->defaultValue(1)->comment('0: folder; 1: file'),
            'path' => $this->string(1300)->notNull()->defaultValue('/')->comment('path logic: e.g( FTP/ABC.rar)'),
            'hash_index' => $this->string(40)->notNull()->comment('hash_index: e.g( SHA1(owner_id.path))'),
            'owner_id' => $this->bigInteger()->notNull()->comment('user_id'),
            'pid' => $this->bigInteger(),
            'size' => $this->bigInteger(),
            'mimetype' => $this->string(),
            'downloadcount' => $this->integer()->defaultValue(0),
            'deleted' => $this->integer()->defaultValue(0)->comment('1:user, 2:cron, 3:inside, 4:lost file, 5:xxx'),
            'description' => $this->string(500)->defaultValue(''),
            'created' => $this->integer(),
            'lastdownload' => $this->integer(),
            'pwd' => $this->string(40)->comment('file password (SHA1)'),
            'modified' => $this->integer(),
            'ip_uploader' => $this->string(45),
        ], $tableOptions);

        $this->createIndex('name', self::TABLE_NAME, 'name');
        $this->createIndex('owner_id', self::TABLE_NAME, 'owner_id');
        $this->createIndex('pid', self::TABLE_NAME, 'pid');
        $this->createIndex('hash_index', self::TABLE_NAME, 'hash_index');
        // $this->createIndex('path', self::TABLE_NAME, 'path'); // TokuDB only
        $this->createIndex('created', self::TABLE_NAME, 'created');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
