<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_levels`.
 */
class m170831_115754_create_user_levels_table extends Migration
{
    const TABLE_NAME = '{{%user_levels}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'COLLATE utf8mb4_general_ci';
        }

        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'time_del_file' => $this->string(),
            'max_upload' => $this->string(),
            'capacity_secure' => $this->string(),
            'capacity_unsecure' => $this->string(),
            'concurrent_dl' => $this->boolean(),
            'time_wait_dl' => 'tinyint(2)',
            'speed_dl' => $this->string(),
            'resume_dl' => $this->boolean(),
            'thread_dl' => $this->string(),
            'access_tool' => $this->boolean(),
            'auto_dl' => $this->boolean(),
            'pwd_link' => $this->boolean(),
            'promotion_info' => $this->boolean()->comment('co ap dung khuyen mai'),
            'bonus_point' => $this->boolean()->unsigned()->defaultValue(0)->comment('Chinh sach diem thuong'),
        ]);

        $this->batchInsert(self::TABLE_NAME, [
            'id', 'name', 'time_del_file', 'max_upload', 'capacity_secure', 'capacity_unsecure',
            'concurrent_dl', 'time_wait_dl', 'speed_dl', 'resume_dl', 'thread_dl', 'access_tool',
            'auto_dl', 'pwd_link', 'promotion_info', 'bonus_point',
        ], [
            [-1, 'Guest', '7 days', '209715200', '0', '0', 0, 30, '150', 1, '0', 0, 0, 0, 1, 0],
            [1, 'Waiting', '0 days', '0', '0', '0', 0, 0, '2', 0, '1', 0, 0, 0, 1, 0],
            [2, 'Member', '17 days', '0', '0', '53687091200', 0, 1, '250', 1, '0', 1, 0, 1, 1, 1],
            [3, 'Vip', '32 days', '0', '53687091200', '268435456000', 1, 0, '0', 1, '1', 1, 1, 1, 1, 1],
            [4, 'SuperVip', '30 days', '0', '53687091200', '1020054732800', 1, 0, '10240', 1, '0', 1, 1, 1, 1, 1],
            [5, 'Vip', '15 days', '0', '42949672960', '0', 1, 0, '2', 1, '1', 1, 1, 1, 1, 1],
            [6, 'VipPoint', '32 days', '0', '53687091200', '268435456000', 1, 0, '10240', 1, '0', 1, 1, 1, 1, 1],
            [7, 'Promo', '32 days', '0', '53687091200', '268435456000', 1, 0, '0', 1, '0', 1, 1, 1, 1, 0],
            [8, 'PromoPlus', '32 days', '0', '53687091200', '268435456000', 1, 0, '10240', 1, '0', 1, 1, 1, 1, 1],
            [9, 'Subscription 15K', '17 days', '0', '10737418240', '53687091200', 0, 0, '10240', 1, '0', 1, 1, 1, 1, 1],
            [10, 'Subscription 10K', '17 days', '0', '10737418240', '53687091200', 0, 0, '10240', 1, '0', 1, 0, 1, 1, 1],
            [11, 'Bundle', '32 days', '0', '53687091200', '268435456000', 1, 0, '8192', 1, '0', 1, 1, 1, 1, 0],
            [12, 'ADSL2plus', '32 days', '0', '10737418240', '10737418240', 1, 0, '8192', 1, '0', 1, 1, 1, 1, 0],
            [13, 'FBOX', '32 days', '0', '10737418240', '10737418240', 1, 0, '3072', 1, '1', 1, 1, 1, 1, 0],
            [14, 'Section', '15 days', '0', '10485760', '53687091200', 1, 0, '1', 1, '1', 1, 1, 1, 1, 0],
            [15, 'SectionPoint', '32 days', '0', '10485760', '53687091200', 1, 0, '1', 1, '1', 1, 1, 1, 1, 0],
            [16, 'Subscription 20K', '17 days', '0', '10737418240', '53687091200', 0, 0, '10240', 1, '0', 1, 1, 1, 1, 1],
            [17, 'Storage', '0 days', '0', '0', '0', 1, 0, '5120', 1, '1', 1, 1, 1, 1, 1],
            [18, 'Forever', '0 days', '0', '53687091200', '268435456000', 1, 0, '15360', 1, '1', 1, 1, 1, 1, 1],
            [19, 'Fsub2K', '30 days', '0', '10737418240', '53687091200', 0, 0, '10240', 1, '0', 1, 1, 1, 1, 1],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        return false;
    }
}
