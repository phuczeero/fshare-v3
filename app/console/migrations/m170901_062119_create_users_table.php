<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170901_062119_create_users_table extends Migration
{
    const TABLE_NAME = '{{%users}}';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'COLLATE utf8mb4_general_ci';
        }

        $this->createTable(self::TABLE_NAME, [
            'id' => $this->bigPrimaryKey(),
            'level' => 'tinyint not null default 1',
            'name' => $this->string()->notNull(),
            'password' => $this->string()->notNull()->defaultValue(''),
            'email' => $this->string()->unique()->notNull()->defaultValue(''),
            'joindate' => $this->integer()->notNull()->defaultValue(0),
            'auth' => 'tinyint not null default 0 comment "0:fshare  1:google  2:facebook"',
            'totalpoints' => $this->double()->unsigned()->notNull()->defaultValue(0),
            'lastvisit' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'activationkey' => $this->string()->notNull()->defaultValue(''),
            'expire_vip' => $this->integer()->unsigned()->notNull()->defaultValue(0)->comment('ngay het han cua vip account'),
            'last_active' => $this->integer()->unsigned()->defaultValue(0),
            'transaction_id' => $this->bigInteger()->unsigned()->defaultValue(0),
            'first_active' => $this->integer()->defaultValue(0),
            'phone' => $this->string()->defaultValue(''),
            'birthday' => $this->date(),
            'gender' => 'tinyint',
            'id_card' => $this->char(50),
            'address' => $this->string(),
            'city' => $this->char(50),
            'country' => $this->string(5),
            'occupation' => $this->char(50),
            'invitor' => $this->bigInteger()->defaultValue(0),
            'partner_id' => $this->integer()->defaultValue(0)->comment('0: fshare; 1: appstore; 28: tech24'),
            'password_tmp' => $this->string(),
            'traffic' => $this->bigInteger()->defaultValue(0)->comment('luu traffic user Vip mua'),
            'traffic_used' => $this->bigInteger()->defaultValue(0)->comment('luu traffic user Vip da dung'),
            'invite_active_type' => 'tinyint default 0 comment "1-mua TK, 2- KH SMS,3-download,4-upload, 34- download_upload, 43-upload_download, 31-download_Mua Tk,41-upload_mua Tk, 32-download_SMS, 42-upload_SMS,0- chua cong diem"',
            'date_telesale_prepaid' => $this->integer()->defaultValue(0)
                ->comment('0: chua danh dau; != 0: la time danh dau cua telesale'),
            'status_telesale_prepaid' => $this->integer()->defaultValue(0),
            'webspace' => $this->bigInteger()->notNull()->defaultValue(0)->comment('dung luong khong dam bao'),
            'webspace_used' => $this->bigInteger()->notNull()->defaultValue(0),
            'webspace_secure' => $this->bigInteger()->notNull()->defaultValue(0)->comment('dung luong dam bao'),
            'webspace_secure_used' => $this->bigInteger()->notNull()->defaultValue(0),
            'bandwidth' => $this->bigInteger()->unsigned()->defaultValue(0)->comment('luu luong toc do cao Trial'),
            'bandwidth_used' => $this->bigInteger()->unsigned()->defaultValue(0)->comment('luu luong toc do cao Trial da dung'),
            'clear_session_time' => $this->integer()->defaultValue(0),
            'amount' => $this->bigInteger()->notNull()->defaultValue(0),
            'brandname' => $this->char(50),
            'setting' => $this->string(755)
                ->defaultValue('{"directdownload":0,"clear_sessiondownload":1364798557,"language":"vi","secure":"0","ispublic":"1"}')
                ->comment('user_direct_download,user_partner_id,user_special,user_uploader'),
            'profile' => $this->string(755)->comment('user_profile (google, facebook)'),
            'telesale_prepaid' => $this->string(),
            'fb_liked_date' => $this->integer(),
            'fb_liked_count_dl' => 'tinyint(2) default 0',
            'status_tpb' => $this->string(200)->defaultValue(0),
            'downloadcount' => $this->integer()->defaultValue(0)->comment('Tong so file download'),
            'uploadcount' => $this->integer()->defaultValue(0)->comment('Tong so file upload'),
            'notificaticate' => 'bit(1) default 1',
            'fellow' => $this->integer()->unsigned()->notNull()->defaultValue(0)->comment('ngay tro thanh hoi vien'),
            'dl_time_avail' => $this->smallInteger(5)->unsigned()->notNull()->defaultValue(0)
                ->comment('so lan duoc phep download con lai'),
        ], $tableOptions);

        $this->createIndex('user_activationkey', self::TABLE_NAME, 'activationkey');
        $this->createIndex('expire_vip', self::TABLE_NAME, 'expire_vip');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        return false;
    }
}
