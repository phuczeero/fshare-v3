<?php
namespace app\components;

use common\models\forms\SignupForm;

class SignupWidget extends \yii\base\Widget
{
    public $model;

    public function init()
    {
        parent::init();
        if ($this->model === null) {
            $this->model = new SignupForm;
        }
    }

    public function run()
    {
        return $this->render('signup', ['model' => $this->model]);
    }
}
