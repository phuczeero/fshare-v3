<?php
namespace app\components;

class AuthButtonWidget extends \yii\base\Widget
{
    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('_auth-button');
    }
}
