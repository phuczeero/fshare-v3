<?php

use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;

$tosLink = Html::a(Yii::t('app', 'Terms of Service'), '#', ['id' => 'tos-dialog-btn']);
$tosLabel = Yii::t('app', 'I agree to the {tos}', ['tos' => $tosLink]);
?>

<div class="widget-signup">
    <div class="mdc-layout-grid mdc-elevation--z4">
        <div class="mdc-layout-grid__inner">
            <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
                <div align="center">
                    <h3 class="box-title"><?= Yii::t('app', 'Sign up') ?></h3>
                    <?= Yii::t('app', 'with your social network') ?>
                </div>

                <?= $this->render('_auth-button') ?>

                <div class="divider"><?= Yii::t('app', 'or') ?></div>

                <?php
                    $form = ActiveForm::begin([
                        'id' => 'form-signup',
                        'action' => ['site/signup'],
                        'options' => ['novalidate' => 'novalidate'],
                    ]);
                ?>
            
                    <?= $form->field($model, 'email')->input('email') ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className()) ?>

                    <?php
                        echo $form
                            ->field($model, 'tos', ['options' => ['class' => 'mdc-form-field full-width field-margin']])
                            ->checkbox(['class' => 'mdc-checkbox__native-control', 'id' => 'tos-checkbox'])
                            ->label($tosLabel);
                    ?>

                    <p><?= Html::submitButton('Signup', ['class' => 'mdc-button mdc-button--raised full-width']) ?></p>
                    
                    <p><?= Yii::t('app', 'Already have an account?') ?> <?= Html::a(Yii::t('app', 'Login now'), ['site/login']) ?></p>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

    <?= $this->render('_tos-dialog') ?>
</div>

<?php
$this->registerJs("
(function() {
    var dialog = new mdc.dialog.MDCDialog($('#tos-dialog')[0]);
    dialog.listen('MDCDialog:accept', function() {
        $('#tos-checkbox').prop('checked', true);
    });
    
    $('#tos-dialog-btn').click(function(e) {
        e.preventDefault();
        dialog.show();
    });
})();
");