<?php

use yii\authclient\widgets\AuthChoice;
?>

<?php $authAuthChoice = AuthChoice::begin([
    'baseAuthUrl' => ['site/auth']
]); ?>

<div class="mdc-layout-grid auth-button-grid">
    <div class="mdc-layout-grid__inner">
        <?php foreach ($authAuthChoice->getClients() as $client): ?>
        <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-6">
            <?php
                $class = $client->getId() === 'google' ? 'google-plus' : $client->getId();
                $icon = '<i class="mdi mdi-' . $class . ' mdc-button__icon"></i>';
                $text = $icon . $client->getName();
                echo $authAuthChoice->clientLink($client, $text, [
                    'class' => 'mdc-button mdc-button--raised full-width ' . $client->getId()
                ]);
            ?>
        </div>
        <?php endforeach; ?>
    </div>
</div>

<?php AuthChoice::end(); ?>
