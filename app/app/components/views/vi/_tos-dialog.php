<aside id="tos-dialog" class="mdc-dialog">
<div class="mdc-dialog__surface">
    <header class="mdc-dialog__header">
        <h2 class="mdc-dialog__header__title">Điều khoản của dịch vụ</h2>
    </header>
    <section class="mdc-dialog__body mdc-dialog__body--scrollable">
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore doloremque quas laborum quaerat ullam cum dolor neque nam beatae quo molestiae eveniet error maiores provident distinctio maxime rerum, at omnis?</p>
    </section>
    <footer class="mdc-dialog__footer">
        <button type="button" class="mdc-button mdc-dialog__footer__button mdc-dialog__footer__button--cancel">Decline</button>
        <button type="button" class="mdc-button mdc-dialog__footer__button mdc-dialog__footer__button--accept">Accept</button>
    </footer>
</div>
<div class="mdc-dialog__backdrop"></div>
</aside>