<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'container' => require(__DIR__ . '/container.php'),
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'converter' => [
                // 'forceConvert' => true,
                'commands' => [
                    'scss' => ['css', 'sass --no-cache --default-encoding UTF-8 {from} {to}'],
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app', 'httpOnly' => true],
        ],
        'session' => [
            'class' => 'yii\mongodb\Session',
            'name' => 'fshare-app',
            'sessionCollection' => 'login_session',
            'timeout' => 3600 * 6,
            'gCProbability' => 0,
            'writeCallback' => function ($session) {
                return [
                    'user_id' => Yii::$app->user->id,
                    'ip_address' => Yii::$app->request->userIP,
                    'user_agent' => Yii::$app->request->userAgent,
                    'updated_at' => time(),
                ];
            },
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'cache' => false,
            'rules' => [
                'account/active' => 'user/active',
                '<controller>/<action>' => '<controller>/<action>',
            ],
        ],
    ],
    'params' => $params,
];
