<?php
return [
    'definitions' => [
        'yii\bootstrap\ActiveForm' => [
            'validateOnBlur' => false,
            'validateOnChange' => false,
            'errorCssClass' => 'mdc-textfield--invalid',
        ],
        'yii\bootstrap\ActiveField' => [
            'inputOptions' => ['class' => 'mdc-textfield__input'],
            'labelOptions' => ['class' => 'mdc-textfield__label'],
            'errorOptions' => ['class' => 'mdc-textfield-helptext mdc-textfield-helptext--validation-msg'],
            'options' => ['class' => 'mdc-textfield full-width', 'data-mdc-auto-init' => 'MDCTextfield'],
            'selectors' => ['error' => '+.mdc-textfield-helptext'], // trick
            'template' => '{input} {label}</div>{error}<div>',
            'checkboxTemplate' => '
                <div class="mdc-checkbox">
                    {input}
                    <div class="mdc-checkbox__background">
                        <svg class="mdc-checkbox__checkmark" viewBox="0 0 24 24">
                            <path class="mdc-checkbox__checkmark__path" fill="none" stroke="white" d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                        </svg>
                        <div class="mdc-checkbox__mixedmark"></div>
                    </div>
                </div>
                {label}</div>{error}<div>
            ',
        ],
        'yii\captcha\Captcha' => [
            'template' => '{input} {image}',
            'options' => ['class' => 'mdc-textfield__input'],
            'imageOptions' => ['style' => 'cursor: pointer']
        ],
        'yii\captcha\CaptchaAction' => [
            'foreColor' => 0xCD1417,
            'height' => 27,
            'width' => 99,
            'offset' => 0,
        ],
    ],
];
