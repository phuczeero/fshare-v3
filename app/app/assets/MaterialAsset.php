<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Material components web asset bundle
 */
class MaterialAsset extends AssetBundle
{
    public $sourcePath = '@npm/material-components-web';
    public $css = [
        '//cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css',
        '//fonts.googleapis.com/css?family=Roboto:300,400,500,700',
        '//fonts.googleapis.com/icon?family=Material+Icons',
        'dist/material-components-web.min.css',
    ];
    public $js = [
        'dist/material-components-web.min.js',
    ];
    public $depends = [
        'app\assets\MaterialDesignIconsAsset',
    ];
}
