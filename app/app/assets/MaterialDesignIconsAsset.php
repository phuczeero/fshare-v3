<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * MDI asset bundle
 */
class MaterialDesignIconsAsset extends AssetBundle
{
    public $sourcePath = '@npm/mdi';
    public $css = [
        'css/materialdesignicons.min.css',
    ];
}
