<?php

/* @var $this yii\web\View */
use app\components\SignupWidget;

$this->title = Yii::t('app', 'Home');
?>

<div class="site-login">
    <?= SignupWidget::widget() ?>
</div>