<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use app\components\SignupWidget;

$this->title = Yii::t('app', 'Sign up');
?>

<div class="site-signup">
    <?= SignupWidget::widget(['model' => $model]) ?>
</div>
