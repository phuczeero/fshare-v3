<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use app\components\AuthButtonWidget;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Login');
?>
<div class="site-login">

<div class="mdc-layout-grid mdc-elevation--z4">
    <div class="mdc-layout-grid__inner">
        <div class="mdc-layout-grid__cell mdc-layout-grid__cell--span-12">
            <div align="center">
                <h3 class="box-title"><?= $this->title ?></h3>
                <?= Yii::t('app', 'with your social network') ?>
            </div>

            <?= AuthButtonWidget::widget() ?>

            <div class="divider"><?= Yii::t('app', 'or') ?></div>

            <?php
                $form = ActiveForm::begin([
                    'id' => 'form-signup',
                    // 'enableAjaxValidation' => true,
                    'options' => ['novalidate' => 'novalidate'],
                ]);
            ?>
        
                <?= $form->field($model, 'email')->input('email') ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?php
                    echo $form
                        ->field($model, 'rememberMe', ['options' => ['class' => 'mdc-form-field field-margin']])
                        ->checkbox(['class' => 'mdc-checkbox__native-control']);
                ?>

                <!-- <div style="color:#999;margin:1em 0">
                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                </div> -->

                <p><?= Html::submitButton('Signup', ['class' => 'mdc-button mdc-button--raised full-width']) ?></p>
                
                <p><?= Yii::t('app', 'Not a member yet?') ?> <?= Html::a(Yii::t('app', 'Sign up now'), ['site/signup']) ?></p>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
