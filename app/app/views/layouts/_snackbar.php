<div id="msg-snackbar" class="mdc-snackbar mdc-snackbar--align-start">
    <div class="mdc-snackbar__text"></div>
    <div class="mdc-snackbar__action-wrapper">
        <button type="button" class="mdc-button mdc-snackbar__action-button"></button>
    </div>
</div>

<?php
$msg = Yii::$app->session->getFlash('error');
$actionText = Yii::t('app', 'Close');
$this->registerJs("
(function() {
    var snackbar = new mdc.snackbar.MDCSnackbar($('#msg-snackbar')[0]);
    var msg = '$msg';
    if (msg) {
        snackbar.show({
            message: msg,
            actionText: '$actionText',
            multiline: true,
            timeout: 7000,
            actionHandler: function () {
                
            }
        });
    }
})();
");