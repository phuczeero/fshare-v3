<?php

use app\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="mdc-typography">
    <?php $this->beginBody() ?>

    <header class="fs mdc-toolbar mdc-toolbar--fixed">
        <div class="mdc-toolbar__row">
            <section class="mdc-toolbar__section mdc-toolbar__section--align-start">
                <span class="mdc-toolbar__title mdc-theme--primary">Fshare</span>
            </section>
            <section class="mdc-toolbar__section mdc-toolbar__section--align-end">
                <?php
                    echo \yii\widgets\Menu::widget([
                        'options' => ['tag' => false],
                        'itemOptions' => ['tag' => false],
                        'linkTemplate' => '<a class="mdc-button" href="{url}">{label}</a>',
                        'items' => [
                            ['label' => Yii::t('app', 'Home'), 'url' => ['site/index']],
                            ['label' => Yii::t('app', 'About'), 'url' => ['site/index']],
                            [
                                'label' => Yii::t('app', 'Login'),
                                'url' => ['site/login'],
                                'visible' => Yii::$app->user->isGuest,
                                'template' => '<a class="mdc-button mdc-button--raised" href="{url}">{label}</a>',
                            ],
                        ],
                    ]);
                ?>
            </section>
        </div>
    </header>
    <main class="mdc-toolbar-fixed-adjust">
        <?= $content ?>
    </main>

    <?= $this->render('_snackbar') ?>

    <?php $this->endBody() ?>
    <script>window.mdc.autoInit();</script>
</body>
</html>
<?php $this->endPage() ?>
