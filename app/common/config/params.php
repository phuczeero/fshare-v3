<?php
return [
    'supportedLanguages' => ['vi', 'en'],
    'user.passwordResetTokenExpire' => 3600,
    'user.activationTokenExpire' => 3600,
    'error.code.createUser' => 1000,
    'error.code.createToken' => 1001,
];
