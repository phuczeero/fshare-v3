<?php
namespace common\components;

use common\models\Auth;
use common\models\Level;
use common\models\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler
{
    const TMP_EMAIL_SUFFIX = '.fshare.vn';

    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->getUserAttributes();
        $id = ArrayHelper::getValue($attributes, 'id');
        $email = ArrayHelper::getValue($attributes, 'email');
        $name = ArrayHelper::getValue($attributes, 'name');
        if ($this->client->getId() === 'google') {
            $email = ArrayHelper::getValue($attributes, 'emails.0.value');
            $name = ArrayHelper::getValue($attributes, 'displayName');
        }

        $auth = Auth::find()->where([
            'source' => $this->client->getId(),
            'source_id' => $id,
        ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                $user = $auth->user;
                if ($this->isTmpEmail($user->email)) {
                    Yii::$app->session->setFlash('isTmpEmail', true);
                }

                Yii::$app->user->login($user);
            } else { // signup
                if ($email !== null && User::find()->where(['email' => $email])->exists()) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Your {client} email already exists as an account. Login using email first and link it', ['client' => $this->client->getTitle()]));
                } else {
                    $user = new User();
                    if ($email === null) {
                        $email = $this->generateTmpEmail();
                        Yii::$app->session->setFlash('isTmpEmail', true);
                    }

                    $user->email = $email;
                    $user->name = $name;
                    $user->generateAuthKey();
                    $user->setUserToLevel(Level::ID_WAITING);

                    $transaction = User::getDb()->beginTransaction();

                    if ($user->save()) {
                        $auth = new Auth([
                            'user_id' => $user->id,
                            'source' => $this->client->getId(),
                            'source_id' => (string)$id,
                        ]);
                        if ($auth->save()) {
                            $transaction->commit();
                            Yii::$app->user->login($user);
                        } else {
                            Yii::$app->session->setFlash('error', Yii::t('app', 'Unable to save {client} account: {errors}', [
                                'client' => $this->client->getTitle(),
                                'errors' => json_encode($auth->getErrors()),
                            ]));
                        }
                    } else {
                        Yii::$app->session->setFlash('error', Yii::t('app', 'Unable to save user: {errors}', [
                            'client' => $this->client->getTitle(),
                            'errors' => json_encode($user->getErrors()),
                        ]));
                    }
                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Auth([
                    'user_id' => Yii::$app->user->id,
                    'source' => $this->client->getId(),
                    'source_id' => (string)$id,
                ]);
                if ($auth->save()) {
                    $user = $auth->user;
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Linked {client} account.', [
                        'client' => $this->client->getTitle()
                    ]));
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Unable to link {client} account: {errors}', [
                        'client' => $this->client->getTitle(),
                        'errors' => json_encode($auth->getErrors()),
                    ]));
                }
            } else { // there's existing auth
                Yii::$app->session->setFlash('error', Yii::t('app', 'Unable to link {client} account. There is another user using it.', [
                    'client' => $this->client->getTitle()
                ]));
            }
        }
    }

    private function generateTmpEmail()
    {
        $attributes = $this->client->getUserAttributes();
        $id = ArrayHelper::getValue($attributes, 'id');
        return $id . '_' . Yii::$app->security->generateRandomString(6) . '@' . $this->client->getId() . self::TMP_EMAIL_SUFFIX;
    }

    public function isTmpEmail($email)
    {
        $parts = explode('@', $email);
        if (count($parts) !== 2) {
            return false;
        }

        $attributes = $this->client->getUserAttributes();
        $id = ArrayHelper::getValue($attributes, 'id');
        $tmpId = explode('_', $parts[0])[0];

        return $id === $tmpId && $parts[1] === $this->client->getId() . self::TMP_EMAIL_SUFFIX;
    }
}
