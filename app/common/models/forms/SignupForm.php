<?php
namespace common\models\forms;

use common\models\Level;
use common\models\User;
use Yii;

/**
 * Signup form
 */
class SignupForm extends \yii\base\Model
{
    public $email;
    public $password;
    public $verifyCode;
    public $tos;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::className()],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['verifyCode', 'captcha', 'when' => function($model) {
                return Yii::$app->id === 'app';
            }],

            ['tos', 'required', 'requiredValue' => 1, 'when' => function($model) {
                return Yii::$app->id === 'app';
            }, 'message' => Yii::t('app', 'Please agree to the terms of service.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'verifyCode' => Yii::t('app', 'Verification code'),
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User;
        $user->email = $this->email;
        $user->name = $user->generateNameFromEmail($this->email);
        $user->setPassword($this->password);
        $user->setUserToLevel(Level::ID_WAITING);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }

    /**
     * Sends an activation email with token.
     *
     * @param User $user the user's model
     * @return bool whether the email was send
     */
    public function sendEmail($user)
    {
        if (!$user) {
            return false;
        }

        if (!User::isActivationTokenValid($user->activation_token)) {
            $user->generateActivationToken();
            if (!$user->save()) {
                return false;
            }
        }

        try {
            $urlManager = Yii::$app->id === 'api' ? Yii::$app->urlManagerWeb : Yii::$app->urlManager;
            $activeLink = $urlManager->createAbsoluteUrl(['account/active', 'token' => $user->auth_key]);
            return Yii::$app->mailer
                ->compose(
                    ['html' => 'signup-html', 'text' => 'signup-text'],
                    ['user' => $user, 'activeLink' => $activeLink]
                )
                ->setFrom([Yii::$app->params['noReplyEmail'] => Yii::$app->params['appName']])
                ->setTo($this->email)
                ->setSubject(Yii::t('app/mail', 'Activation of your account'))
                ->send();
        } catch (\Swift_TransportException $e) {
            return false;
        }

    }
}
