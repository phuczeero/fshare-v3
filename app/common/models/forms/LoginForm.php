<?php
namespace common\models\forms;

use common\models\Token;
use common\models\User;
use Yii;

/**
 * Login form
 */
class LoginForm extends \yii\base\Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],

            ['password', 'required'],
            ['password', 'validatePassword'],

            ['rememberMe', 'boolean'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->validatePasswordWithCondition()) {
                $this->addError($attribute, 'Incorrect email or password.');
            }
        }
    }

    /**
     * Extracted method which use to validate v2 password and v3 password
     *
     * @return bool whether the password is valid
     */
    public function validatePasswordWithCondition()
    {
        $user = $this->getUser();
        if (!$user) {
            return false;
        }

        if (empty($user->password_hash)) {
            if ($user->validateOldPassword($this->password)) {
                $user->setPassword($this->password);
                return $user->save();
            }

            return false;
        }

        return $user->validatePassword($this->password);
    }

    /**
     * Logs in a user using the provided email and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
    }

    /**
     * Logs in a user without session.
     *
     * @return Token|null the saved model or null if saving fails
     */
    public function loginStateless()
    {
        do {
            $id = Yii::$app->security->generateRandomString();
            $token = Token::findOne(['id' => $id]);
        } while ($token !== null);

        $token = new Token;
        $token->id = $id;
        $user = $this->getUser();
        $token->user_id = $user->id;
        $token->setClientData();

        return $token->save() ? $token : null;
    }

    /**
     * Finds user by email
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }

    /**
     * Set user identity
     *
     * @param User $user
     */
    public function setUser($user)
    {
        if ($user) {
            $this->_user = $user;
        }
    }
}
