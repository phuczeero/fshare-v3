<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%user_levels}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $time_del_file
 * @property string $max_upload
 * @property string $capacity_secure
 * @property string $capacity_unsecure
 * @property integer $concurrent_dl
 * @property integer $time_wait_dl
 * @property string $speed_dl
 * @property integer $resume_dl
 * @property string $thread_dl
 * @property integer $access_tool
 * @property integer $auto_dl
 * @property integer $pwd_link
 * @property integer $promotion_info
 * @property integer $bonus_point
 */
class Level extends \yii\db\ActiveRecord
{
    const ID_WAITING = 1;
    const ID_MEMBER = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_levels}}';
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'name' => function ($model) {
                return strtoupper($model->name);
            },
        ];
    }

    /**
     * Finds level by id
     *
     * @param int $id
     * @return static|null
     */
    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }
}
