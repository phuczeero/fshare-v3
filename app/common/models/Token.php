<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\mongodb\ActiveRecord;

/**
 * Token model
 *
 * @property string $_id
 * @property string $id
 * @property int $user_id
 * @property int $updated_at
 * @property string $ip_address
 * @property string $user_agent
 */
class Token extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'login_session';
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['_id', 'id', 'user_id', 'updated_at', 'ip_address', 'user_agent'];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => false,
            ]
        ];
    }

    /**
     * Set client data to token document
     */
    public function setClientData()
    {
        $request = Yii::$app->request;
        $this->ip_address = $request->userIP;
        $this->user_agent = $request->userAgent;
    }
}
