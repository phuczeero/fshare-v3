<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property integer $level
 * @property string $name
 * @property string $password
 * @property string $email
 * @property integer $joindate
 * @property integer $auth //remove
 * @property integer $totalpoints
 * @property integer $lastvisit //remove
 * @property string $activationkey //remove
 * @property integer $expire_vip
 * @property integer $last_active //remove
 * @property integer $transaction_id //remove
 * @property integer $first_active //remove
 * @property string $phone //move
 * @property string $birthday //move
 * @property integer $gender //move
 * @property string $id_card //move
 * @property string $address //move
 * @property string $city //move
 * @property string $country //move
 * @property string $occupation //move
 * @property integer $invitor //remove
 * @property integer $partner_id
 * @property string $password_tmp
 * @property integer $traffic
 * @property integer $traffic_used
 * @property integer $invite_active_type //remove
 * @property integer $date_telesale_prepaid //move
 * @property integer $status_telesale_prepaid //move
 * @property integer $webspace
 * @property integer $webspace_used
 * @property integer $webspace_secure
 * @property integer $webspace_secure_used
 * @property integer $bandwidth //remove
 * @property integer $bandwidth_used //remove
 * @property integer $clear_session_time //remove
 * @property integer $amount
 * @property string $brandname //remove
 * @property string $setting //move
 * @property string $profile //remove
 * @property string $telesale_prepaid //remove
 * @property integer $fb_liked_date //remove
 * @property integer $fb_liked_count_dl //remove
 * @property string $status_tpb //remove
 * @property integer $downloadcount
 * @property integer $uploadcount
 * @property integer $notificaticate //move
 * @property integer $fellow //remove
 * @property integer $dl_time_avail
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%users}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'joindate',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        return [
            'id',
            'email',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $token = Token::findOne(['id' => $token]);
        if (!$token) {
            return false;
        } else {
            $token->setClientData();
            $token->save();
        }

        return static::findOne(['id' => $token->user_id]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by activation token
     *
     * @param string $token activation token
     * @return static|null
     */
    public static function findByActivationToken($token)
    {
        if (!static::isActivationTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'activation_token' => $token,
            'level' => Level::ID_WAITING,
        ]);
    }

    /**
     * Finds out if activation token is valid
     *
     * @param string $token activation token
     * @return bool
     */
    public static function isActivationTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.activationTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Active user by token
     *
     * @param  string $token Activation token
     * @return bool Whether user was actived
     */
    public static function activeUser($token)
    {
        $user = static::findByActivationToken($token);
        if (!$user) {
            return false;
        }

        $user->setUserToLevel(Level::ID_MEMBER);
        $user->removeActivationToken();

        return $user->save() ? $user : null;
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }    

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Validates old password of Fv2
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validateOldPassword($password)
    {
        $salt = explode(':', $this->password)[1];
        $hash = md5($password . substr($salt, 0, 11));
        return implode(':', [$hash, $salt]) === $this->password;
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Set level
     *
     * @param int $levelId Level id
     */
    public function setLevel($levelId)
    {
        $this->level = $levelId;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new activation token
     */
    public function generateActivationToken()
    {
        $this->activation_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes activation token
     */
    public function removeActivationToken()
    {
        $this->activation_token = null;
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Extract name from email
     */
    public function generateNameFromEmail($email)
    {
        return StringHelper::explode($email, '@')[0];
    }

    /**
     * Set user attributes to level
     *
     * @param int $levelId Level id
     */
    public function setUserToLevel($levelId)
    {
        $level = Level::findById($levelId);
        if (!$level) {
            return false;
        }

        $this->level = $level->id;
        $this->webspace = $level->capacity_unsecure;
        $this->webspace_secure = $level->capacity_secure;
    }
}
