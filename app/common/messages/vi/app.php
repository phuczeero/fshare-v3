<?php

return [
    'Create an account' => 'Đăng ký',
    'I agree to the {tos}' => 'Đồng ý với {tos}',
    'Terms of Service' => 'Điều khoản của dịch vụ',
    'Please agree to the terms of service.' => 'Vui lòng đồng ý với điều khoản của dịch vụ.',
    'Sign up' => 'Đăng ký',
    'Sign up now' => 'Đăng ký ngay',
    'Login' => 'Đăng nhập',
    'Login now' => 'Đăng nhập ngay',
    'Verification code' => 'Mã xác nhận',
    'Email' => 'Email',
    'Password' => 'Mật khẩu',
    'Already have an account?' => 'Đã có tài khoản',
    'Not a member yet?' => 'Chưa có tài khoản',
    'Close' => 'Đóng',
    'Your {client} email already exists as an account. Login using email first and link it' => 'Email {client} của quý khách đã tồn tại trên hệ thống nhưng chưa được liên kết, vui lòng đăng nhập bằng email và tiến hành liên kết',
];
