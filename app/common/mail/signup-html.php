<?php
use yii\helpers\Html;
?>
<div class="account-active">
    <p>Hello <?=Html::encode($user->name)?>,</p>
    <p>Follow the link below to active your account:</p>
    <p><?=Html::a(Html::encode($activeLink), $activeLink)?></p>
</div>
