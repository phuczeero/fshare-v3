<?php
use yii\helpers\Html;
?>
<div class="account-active">
    <p>Xin chào <?= Html::encode($user->email) ?>,</p>
    <p>Để kích hoạt tài khoản, quý khách vui lòng nhấp vào link dưới đây:</p>
    <p><?= Html::a(Html::encode($activeLink), $activeLink) ?></p>
</div>
