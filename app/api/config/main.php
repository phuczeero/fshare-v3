<?php
$params = array_merge(
    require (__DIR__ . '/../../common/config/params.php'),
    require (__DIR__ . '/../../common/config/params-local.php'),
    require (__DIR__ . '/params.php'),
    require (__DIR__ . '/params-local.php')
);

return [
    'id' => 'api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => [
        'log',
        [
            'class' => 'yii\filters\ContentNegotiator',
            'languages' => $params['supportedLanguages'],
        ],
    ],
    'modules' => [
        'v3' => [
            'class' => 'api\modules\v3\Module',
        ],
    ],
    'components' => [
        'request' => [
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'response' => [
            'format' => yii\web\Response::FORMAT_JSON,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableSession' => false,
            'loginUrl' => null,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'cache' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v3/user'],
                    'only' => ['create', 'update', 'view', 'options'],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v3/token'],
                    'only' => ['create', 'view', 'options', 'index', 'delete'],
                    'tokens' => [
                        '{id}' => '<id:\\w+>',
                    ]
                ],
            ],
        ],
        'urlManagerWeb' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'cache' => false,
            'hostInfo' => $params['hostInfo'],
            'rules' => [
                'account/active' => 'account/active',
            ],
        ],
    ],
    'params' => $params,
];
