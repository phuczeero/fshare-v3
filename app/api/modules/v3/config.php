<?php
return [
    'components' => [
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                $response = $event->sender;
                if (\yii\helpers\ArrayHelper::getValue($response->data, 'errors') !== null) {
                    $response->statusCode = 400;
                }
            },
        ],
    ],
    'params' => [
        // list of parameters
    ],
];
