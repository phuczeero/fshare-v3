<?php

namespace api\modules\v3\controllers;

use common\models\forms\SignupForm;
use common\models\User;
use Yii;
use yii\helpers\Url;

/**
 * User controller for the `v3` module
 */
class UserController extends \yii\rest\ActiveController
{
    public $modelClass = 'common\models\User';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        // Customize default actions
        unset($actions['create']);
        $actions['options']['collectionOptions'] = ['POST', 'OPTIONS'];
        // $actions['options']['resourceOptions'] = ['GET', 'HEAD', 'PUT', 'PATCH', 'OPTIONS'];
        return $actions;
    }

    /**
     * Create a user (sign up).
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SignupForm;
        $model->load(Yii::$app->request->bodyParams, '');
        if (!$model->validate()) {
            return ['errors' => $model->errors];
        }

        $user = $model->signup();
        if ($user === null) {
            throw new ServerErrorHttpException('Create user error', Yii::$app->params['error.code.createUser']);
        }

        // send activation mail
        $mailResult = intval($model->sendEmail($user));

        Yii::$app->response->headers->add('Location', Url::to(['user/view', 'id' => $user->id], true));
        Yii::$app->response->statusCode = 201;
        Yii::$app->response->headers->add('X-Mail-Result', $mailResult);

        return $user;
    }
}
