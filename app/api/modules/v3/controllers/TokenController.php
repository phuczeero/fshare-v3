<?php

namespace api\modules\v3\controllers;

use common\models\forms\LoginForm;
use common\models\Token;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\Url;

/**
 * Token controller for the `v3` module
 */
class TokenController extends \yii\rest\ActiveController
{
    public $modelClass = 'common\models\Token';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions = parent::actions();
        // Customize default actions
        unset($actions['create'], $actions['index']);
        // $actions['options']['resourceOptions'] = ['GET', 'HEAD', 'PUT', 'PATCH', 'OPTIONS'];
        return $actions;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'except' => ['create'],
        ];
        return $behaviors;
    }

    /**
     * Create a token (login).
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LoginForm;
        $model->load(Yii::$app->request->bodyParams, '');
        if (!$model->validate()) {
            return ['errors' => $model->errors];
        }

        $token = $model->loginStateless();
        if ($token === null) {
            throw new ServerErrorHttpException('Create token error', Yii::$app->params['error.code.createToken']);
        }

        Yii::$app->response->headers->add('Location', Url::to(['token/view', 'id' => $token->id], true));
        Yii::$app->response->statusCode = 201;

        return $token;
    }

    public function actionIndex()
    {
        $provider = new \yii\data\ActiveDataProvider([
            'query' => Token::find()->where(['user_id' => Yii::$app->user->id]),
        ]);

        return $provider;
    }
}
