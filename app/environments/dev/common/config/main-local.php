<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql;dbname=fshare',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'redis',
            'port' => 6379,
            'database' => 0,
        ],
        'mongodb' => [
            'class' => 'yii\mongodb\Connection',
            'dsn' => 'mongodb://mongo:27017/fshare',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => '118.69.135.177',
                'username' => 'no-reply.test@fshare.vn',
                'password' => 'vz8rtQWquQ',
                'port' => '25',
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'httpClient' => [
                'transport' => 'yii\httpclient\CurlTransport',
                'requestConfig' => [
                    'options' => [
                        'proxy' => getenv('http_proxy'),
                    ],
                ],
            ],
            'clients' => [
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '223728538494-er74frkrr0e1n7r9skhvt16inmvu0ln5.apps.googleusercontent.com',
                    'clientSecret' => 'lGxV0rTjv1E-YIPo2TIFFkzd',
                ],
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '508382572637061',
                    'clientSecret' => '038309a6638723e3f92f2a7eac980d9f',
                ],
            ],
        ]
    ],
];
